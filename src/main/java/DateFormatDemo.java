import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateFormatDemo {

    private static DateTimeFormatter format1 = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
    private static DateTimeFormatter format2 = DateTimeFormatter.ofPattern("yyyy-mm-dd");

    public static void main(String args[]){
        LocalDateTime input = LocalDateTime.of(2019, 05, 19, 10, 9, 06);
        System.out.println(format2.format(input));
        System.out.println(format1.format(input));
    }
}
